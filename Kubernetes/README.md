﻿Для развертывания запустите:
kubectl apply -f

Будут развернуты сервисы nginx и tomcat (использованы стандартные образы).
- Наружу смотрит только nginx.
- В tomcat развернуто тестовое приложение https://tomcat.apache.org/tomcat-7.0-doc/appdev/sample/sample.war
- В nginx настроены страницы:
    / - страница приветствия nginx,
    /sample - тестовое приложение в tomcat.







