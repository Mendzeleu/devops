#!/bin/bash
kline=$(cat /opt/test_file.txt | wc -l)
while [ $kline -gt 0 ]
do
string_Err=$(sed -n "${kline}p" /opt/test_file.txt)
case "$string_Err" in
*error*)
TIMESTAMP=date +"%Y-%m-%d_%H:%M:%S"
sudo sed -i -e "${kline}s/error/error Alert detected at $TIMESTAMP/g" /opt/test_file.txt
exit 1
;;
esac
kline=$(($kline - 1))
done