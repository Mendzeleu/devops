������ zabbix, ������� ����������� ������������ Docker-����������, ������� �������� ��� ������� ���������� � �������� "������".

## Installation

Install files for Zabbix Agent.

1 Copy the file "docker.sh" to "/etc/zabbix/scripts". Set permissions for Zabbix user to execute this file.
2 Copy the file "docker.conf" to "/etc/zabbix/zabbix_agentd.d"
3 Check if "netcat" and "jq" are installed. Install if not found
4 Restart Zabbix Agent

Install file for Zabbix Server.

1 Import the "docker_active.xml" to Zabbix Server.
