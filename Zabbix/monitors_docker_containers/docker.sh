#!/bin/bash
set -e

# Find netcat command or die
NC_CMD=$(command -v netcat || command -v nc || exit 1)
# Path of docker socket
DOCKER_SOCKET=/var/run/docker.sock

# Chech if docker socket is writable with the current user
if [ -w "$DOCKER_SOCKET" ]; then
  NC="$NC_CMD"
else
  # Current user does not belong to docker group, use sudo (requires that sudo rights given correctly in the system)
  NC="sudo $NC_CMD"
fi

# Executes GET command to docker socket
# Parameters: 1 - docker command
docker_get() {
  RESPONSE=$(printf "GET $1 HTTP/1.0\r\n\r\n" | $NC -U $DOCKER_SOCKET | tail -n 1)
}

# Executes command in docker container
# Parameters: 1 - Container name
#             2 - Command and arguments in quoted comma separated list (e.g. "ls", "-l")
docker_exec() {
  # Create command execution
  local BODY="{\"AttachStdout\": true, \"Cmd\": [$2]}"
  local CREATE_RESPONSE
  CREATE_RESPONSE=$(printf "POST /containers/$1/exec HTTP/1.0\r\nContent-Type: application/json\r\nContent-Length: ${#BODY}\r\n\r\n${BODY}" | $NC -U $DOCKER_SOCKET | tail -n 1)
  local RUN_ID=$(echo $CREATE_RESPONSE | jq ".Id // empty " | sed -e 's/"//g')

  # Start execution
  if [ "$RUN_ID" != "" ]; then
    # tr at end is used to suppress warning on bash >=4.4
    RESPONSE=$(printf "POST /exec/$RUN_ID/start HTTP/1.0\r\nContent-Type: application/json\r\nContent-Length: 2\r\n\r\n{}" | $NC -U $DOCKER_SOCKET | tr -d '\0')
  else
    RESPONSE=""
  fi
}

# Obtains last line from execution of cat file on docker container
# Parameters:: 1 - Container name
#              2 - File in container
cat_single_value() {
  local CMD="\"cat\", \"$2\""
  docker_exec $1 "$CMD"
  local VALUE=$(echo "$RESPONSE" | tail -n 1 | tr -cd "[:print:]")
  echo $VALUE
}

# Docker container discovery
# Parameters: 1 - all or running; defaults to running
discovery() {
  docker_get "/containers/json"
  LEN=$(echo $RESPONSE | jq "length")
  for I in $(seq 0 $((LEN-1)))
  do
      NAME=$(echo "$RESPONSE"|jq --raw-output ".[$I].Names[0]"|sed -e 's/^\///')
      ID=$(echo "$RESPONSE"|jq --raw-output ".[$I].Id")
      IMAGENAME=$(echo "$RESPONSE"|jq --raw-output ".[$I].Image"|sed -e 's/:.*//')
      IMAGETAG=$(echo "$RESPONSE"|jq --raw-output ".[$I].Image"|sed -e 's/.*://')

      DATA="$DATA,"'{"{#CONTAINERNAME}":"'$NAME'","{#CONTAINERID}":"'$ID'","{#IMAGENAME}":"'$IMAGENAME'","{#IMAGETAG}":"'$IMAGETAG'"'

      DATA="$DATA,"'"{#HCONTAINERID}":"'$ID'"}'

  done
  echo '{"data":['${DATA#,}']}'
}

# Statistic: Container memory
memory() {
  NEW_VALUE=$(cat_single_value $1 "/sys/fs/cgroup/memory/memory.usage_in_bytes")
  if [ "$NEW_VALUE" = "" ]; then
    echo "0"
  else
    echo $NEW_VALUE
  fi
}


if [ $# -eq 0 ]; then
  echo "No arguments"
  exit 1
elif [ $# -eq 1 ]; then
  $1
elif [ $# -eq 2 ]; then
  CONT_ID=$(echo "$1" | sed 's/^\///')

  # Execute statistic function with container argument
  $2 "$CONT_ID"
fi

