name 'Lesson_4_httpd'
maintainer 'Siarhei Mendzeleu'
maintainer_email 'smendelev@tut.by'
license 'All Rights Reserved'
description 'Installs/Configures Lesson_4_httpd'
long_description 'Installs/Configures Lesson_4_httpd'
version '0.1.0'
chef_version '>= 13.0'

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
# issues_url 'https://github.com/<insert_org_here>/Lesson_4_httpd/issues'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
# source_url 'https://github.com/<insert_org_here>/Lesson_4_httpd'
