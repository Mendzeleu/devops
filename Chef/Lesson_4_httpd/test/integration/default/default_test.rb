# InSpec test for recipe Lesson_4_httpd::default

# The InSpec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

######## ATTRIBUTES ########
PACKAGE = attribute('package', 
                    default: 'httpd',
                    description: 'package name'
                   )
                   
SERVICE = attribute('service', 
                    default: 'httpd', 
                    description: 'service name'
                   )
######## TEST CONFIGURATION ########
describe package(PACKAGE) do
  it { should be_installed }
end
describe service(SERVICE) do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end
######## TEST PROMISE ########
describe port(8000) do
  it { should be_listening }
end
describe http('localhost:8000') do
  its('status') { should eq 200 }
  its('headers.Content-Type') { should include 'text/html' }
  its('body') { should include 'Hello World!' }
end
