city=$1                      # City (where you want to know the weather forecast)
apikeyfile=$2                # File that contains key API openweathermap
emails=$3                    # Mail to send weather (optional or comma-separated list)
apikey=$(cat $apikeyfile)

DATA=$(curl -s "http://api.openweathermap.org/data/2.5/weather?appid="$apikey"&q="$city)
messagebody="Temperature in kelvin unit="$(echo ${DATA} | jq ".main.temp")", atmospheric pressure in hPa unit = "$(echo ${DATA} | jq ".main.pressure")", humidity in percentage = "$(echo ${DATA} | jq ".main.humidity")

if [ -n "$3" ]
then
    mail -s 'Weather now' $emails <<< "$messagebody"
else
    echo $messagebody
fi
