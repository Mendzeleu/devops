# Program to show current weather

import os

import requests
import json

import argparse

import smtplib
import ssl


# Create url, to show current weather
# city - city (where you want to know the weather forecast)
# apikeyfile - file that contains key API openweathermap
# emails - mail to send weather (optional or comma-separated list)
# base_url - weather forecast site (API)

base_url = "http://api.openweathermap.org/data/2.5/weather?"

parser = argparse.ArgumentParser()
parser.add_argument(
    'city', type=str, help='Where you want to know the weather forecast')
parser.add_argument('apikeyfile', type=str, help='API openweathermap')
parser.add_argument('--emails', type=str, help='Mail to send weather')
arg_all = parser.parse_args()

# Open the file. Read key
api_file = open(arg_all.apikeyfile, "r", encoding="utf-8")
api_key = api_file.read().rstrip()

complete_url = base_url + "appid=" + api_key + "&q=" + arg_all.city


# We get a response from the site. Requests has internal json parser

response = requests.get(complete_url)
rj = response.json()

if rj["cod"] == 200:

    # Find out the temperature, pressure, humidity. Create message text

    city_main = rj["main"]
    message_email = """Subject: weather

    
        Temperature in kelvin unit = """ + str(
        city_main["temp"]) + """, atmospheric pressure in hPa unit = """ + str(
        city_main["pressure"]) + """, humidity in percentage = """ + str(
            city_main["humidity"])
            

    if arg_all.emails:

        # Specify the SMTP Server and Port
        # receiver_email from parameter emails
        # sender_email and password_sender
        # from environment variables GMAIL_USER and GMAIL_PASSWORD

        smtp_server = "smtp.gmail.com"
        port_smtp = 587
        sender_email = os.environ['GMAIL_USER']
        receiver_email = arg_all.emails.split()
        password_sender = os.environ['GMAIL_PASSWORD']

        # Create a secure SSL context. Sends emails

        context = ssl.create_default_context()

        with smtplib.SMTP(smtp_server, port_smtp) as server:
            server.ehlo()
            server.starttls(context=context)
            server.ehlo()
            server.login(sender_email, password_sender)
            server.sendmail(sender_email, receiver_email, message_email)
            server.quit()

    else:

        print(message_email)

else:

    print("Check entered data")
