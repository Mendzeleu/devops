docker build --build-arg ANSIBLE_VERSION=2.6.6 -t mendzeleu/ansible2.6.6 .
docker build --build-arg ANSIBLE_VERSION=2.7.7 -t mendzeleu/ansible2.7.7 .
docker run --rm mendzeleu/ansible2.6.6
docker run --rm mendzeleu/ansible2.7.7
