﻿Bash-скрипт, который создает два docker-образа (один - с Ansible версий 2.6.6, другой - 2.7.7) и запускает их после по умолчанию с ENTRYPOINT/CMD.  Инструкцией по запуску playbook.yml с Docker-контейнерами на основе этих двух Ansible-образов описана ниже.
---------------------------------------------------------------------------------

You can use Ansible 2.6.6 to run playbook.yml (replace "2.6.6" with "2.7.7" and "a266" with "a277" for Ansible 2.7.7).
From the directory in which this file is located, enter this line:
docker run --rm -v $(pwd):/ansible/ mendzeleu/ansible2.6.6 YOUR_PLAYBOOK.YML
Example:
docker run --rm -v $(pwd):/ansible/ mendzeleu/ansible2.6.6 playbook.yml

To be able to get constant possibility just type “a266 playbook.yml”, you need to use one of two ways:

1. You need to add this line to file ~/.bashrc (or ~/.bash_aliases for local user)

alias a266='docker run -P -v $(pwd):/ansible/ mendzeleu/ansible2.6.6'

2. You need to add this function to file ~/.bashrc (or ~/.bash_aliases for local user)

function a266 ()
{
 docker run --rm -v $(pwd):/ansible/ mendzeleu/ansible2.6.6 $1;
}

Execute command:
. ~/.bashrc

After that, “a266 playbook.yml” will work.