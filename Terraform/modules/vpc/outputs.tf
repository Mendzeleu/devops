output "vpc_id" {
  description = "The ID of the VPC"
  value       = aws_vpc.vpc_main.id
}

output "vpc_cidr_block" {
  description = "the VPC cidr block"
  value       = aws_vpc.vpc_main.cidr_block
}

output "subnet_id" {
  description = "The ID of the subnets"
  value       = [aws_subnet.public1.id, aws_subnet.public2.id, aws_subnet.public3.id]
}
