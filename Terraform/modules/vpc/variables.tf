variable "name" {
  description = "Name of the VPC"
  type        = string
  default     = ""
}

variable "cidr_VPC" {
  description = "The CIDR block for the VPC"
  type        = string
}

variable "cidr_subnet1" {
  description = "The CIDR block for the subnet1"
  type        = string
}

variable "cidr_subnet2" {
  description = "The CIDR block for the subnet2"
  type        = string
}

variable "cidr_subnet3" {
  description = "The CIDR block for the subnet3"
  type        = string
}

variable "az_id1" {
  description = "The availability zones id for the subnet1"
  type        = string
}

variable "az_id2" {
  description = "The availability zones id for the subnet2"
  type        = string
}

variable "az_id3" {
  description = "The availability zones id for the subnet3"
  type        = string
}