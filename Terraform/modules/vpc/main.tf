resource "aws_vpc" "vpc_main" {
  cidr_block = var.cidr_VPC

  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = var.name
  }
}

resource "aws_internet_gateway" "default" {
 vpc_id = aws_vpc.vpc_main.id
}

resource "aws_route" "internet_access" {
  route_table_id         = aws_vpc.vpc_main.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.default.id
}

resource "aws_subnet" "public1" {
  vpc_id                  = aws_vpc.vpc_main.id
  cidr_block              =  var.cidr_subnet1
  availability_zone_id = var.az_id1
  map_public_ip_on_launch = true
}

resource "aws_subnet" "public2" {
  vpc_id                  = aws_vpc.vpc_main.id
  cidr_block              = var.cidr_subnet2
  availability_zone_id = var.az_id2
  map_public_ip_on_launch = true
}

resource "aws_subnet" "public3" {
  vpc_id = aws_vpc.vpc_main.id
  cidr_block = var.cidr_subnet3
  availability_zone_id = var.az_id3
  map_public_ip_on_launch = true
}