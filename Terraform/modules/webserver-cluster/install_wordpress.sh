#!/bin/bash
sudo yum update -y
sudo yum install httpd php php-mysql -y
cd /var/www/html
sudo rm -rf *
sudo wget -O wordpress.tar.gz https://wordpress.org/wordpress-4.7.4.tar.gz
sudo tar -xzf wordpress.tar.gz
sudo cp -rf wordpress/* /var/www/html/
sudo rm -rf wordpress
sudo rm -rf wordpress.tar.gz
sudo mv wp-config-sample.php wp-config.php
sudo sed -i "s/database_name_here/${db_name}/g" /var/www/html/wp-config.php
sudo sed -i "s/username_here/${db_username}/g" /var/www/html/wp-config.php
sudo sed -i "s/password_here/${db_password}/g" /var/www/html/wp-config.php
sudo sed -i "s/localhost/${db_address}/g" /var/www/html/wp-config.php
sudo chmod -R 755 wp-content
sudo chown -R apache.apache wp-content
sudo sed -i 's/80/${server_port}/g' /etc/httpd/conf/httpd.conf
sudo service httpd start
sudo chkconfig httpd on
