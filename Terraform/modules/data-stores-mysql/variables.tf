variable "cluster_name" {
  description = "The name to use for all the cluster resources"
  type        = string
}

variable "db_name" {
  description = "The name to use for the database"
  type        = string
}

variable "multi_az" {
  type        = bool
  description = "Set to true if multi AZ deployment must be supported"
}

variable "db_username" {
  description = "The username for the database"
  type        = string
}

variable "db_password" {
  description = "The password for the database"
  type        = string
}

variable "vpc_id" {
  description = "A list of VPC IDs to launch resources"
  type        = string
}

variable "subnet_id" {
  description = "A list of subnet IDs to launch resources"
  type        = list(string)
}

variable "db_allow_ips" {
  description = "Allow ips for DB"
  type        = string
}