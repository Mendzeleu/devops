terraform {
  required_version = ">= 0.12, < 0.13"
}

resource "aws_db_subnet_group" "default" {
  name       = "${var.cluster_name}-db-sg"
  subnet_ids = var.subnet_id

  tags = {
    Name = "DB subnet group"
  }
}

resource "aws_db_instance" "wordpress" {
  identifier_prefix   = "db-instance-wordpress"
  engine              = "mysql"
  allocated_storage   = 10
  instance_class      = "db.t2.micro"
  name                = var.db_name
  username            = var.db_username
  password            = var.db_password
  multi_az            = var.multi_az
  vpc_security_group_ids = [aws_security_group.for_rds.id]
  db_subnet_group_name = aws_db_subnet_group.default.id
  skip_final_snapshot = true
}

resource "aws_security_group" "for_rds" {
  name = "${var.db_name}-rds"
  vpc_id = var.vpc_id
}

resource "aws_security_group_rule" "allow_server_db_inbound" {
  type              = "ingress"
  security_group_id = aws_security_group.for_rds.id

  from_port   = local.db_port
  to_port     = local.db_port
  protocol    = local.tcp_protocol
  cidr_blocks = local.db_ips
}

resource "aws_security_group_rule" "allow_server_db_outbound" {
  type              = "egress"
  security_group_id = aws_security_group.for_rds.id

  from_port   = local.db_port
  to_port     = local.db_port
  protocol    = local.tcp_protocol
  cidr_blocks = local.db_ips
}

locals {
  db_port      = 3306
  tcp_protocol = "tcp"
  db_ips       = [var.db_allow_ips]
}