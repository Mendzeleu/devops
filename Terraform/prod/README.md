﻿Эта папка содержит конфигурацию Terraform для Production, которая развертывает кластер веб-серверов в созданной VPC (схема в файле Production.png), используя EC2, Auto Scaling, балансировщик нагрузки ELB (прослушивает порт 80) и Amazon Relational Database Service c развертыванием в нескольких зонах доступности, используя Multi-AZ Deployment, в учетной записи Amazon Web Services. Добавлены ресурсы "aws_autoscaling_schedule" управляющие количеством серверов в заданные часы. Схема развертывания в файле Production.png


Для экспорта пароля для БД введите:

export TF_VAR_db_password="(YOUR_DB_PASSWORD)"

или при развертывании будет предложено его ввести вручную

Разверните код, используя команды:

terraform init
terraform apply

