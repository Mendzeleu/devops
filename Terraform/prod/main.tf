terraform {
  required_version = ">= 0.12, < 0.13"
}

provider "aws" {
  region = "us-east-2"

  # Allow any 2.x version of the AWS provider
  version = "~> 2.0"
}

module "vpc" {
  source = "../modules/vpc/"

  name = "prod_vpc"

  cidr_VPC = "10.10.0.0/20"
  cidr_subnet1 = "10.10.11.0/24"
  cidr_subnet2 = "10.10.12.0/24"
  cidr_subnet3 = "10.10.13.0/24"
  az_id1 = "use2-az1"
  az_id2 = "use2-az2"
  az_id3 = "use2-az3"
}

module "mysql" {
  source = "../modules/data-stores-mysql/"

  cluster_name = var.cluster_name

  db_name     = var.db_name
  db_username = var.db_username
  db_password = var.db_password
  multi_az    = var.multi_az
  vpc_id = module.vpc.vpc_id
  subnet_id = module.vpc.subnet_id
  db_allow_ips = module.vpc.vpc_cidr_block
}

module "webserver_cluster" {

  source = "../modules/webserver-cluster/"

  cluster_name = var.cluster_name

  instance_type = "t2.micro"
  min_size      = 3
  max_size      = 10
 subnet_id = module.vpc.subnet_id
  vpc_id = module.vpc.vpc_id
  db_address = module.mysql.address
  db_name     = var.db_name
  db_username = var.db_username
  db_password = var.db_password
}

resource "aws_autoscaling_schedule" "scale_out_during_business_hours" {
  scheduled_action_name  = "scale-out-during-business-hours"
  min_size               = 2
  max_size               = 10
  desired_capacity       = 10
  recurrence             = "0 9 * * *"
  autoscaling_group_name = module.webserver_cluster.asg_name
}

resource "aws_autoscaling_schedule" "scale_in_at_night" {
  scheduled_action_name  = "scale-in-at-night"
  min_size               = 2
  max_size               = 10
  desired_capacity       = 2
  recurrence             = "0 17 * * *"
  autoscaling_group_name = module.webserver_cluster.asg_name
}
