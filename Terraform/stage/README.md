﻿Эта папка содержит конфигурацию Terraform для Staging, которая развертывает кластер веб-серверов в созданной VPC (схема в файле Staging.png), используя EC2, Auto Scaling, балансировщик нагрузки ELB (прослушивает порт 80) и Amazon Relational Database Service, в учетной записи Amazon Web Services. Схема развертывания в файле Staging.png


Для экспорта пароля для БД введите:

export TF_VAR_db_password="(YOUR_DB_PASSWORD)"

или при развертывании будет предложено его ввести вручную

Разверните код, используя команды:

terraform init
terraform apply