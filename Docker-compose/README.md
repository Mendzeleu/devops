﻿Запустите:
docker-compose up -d
Приложение станет доступно по ссылке http://localhost/sample/

./backend.Dockerfile - Dockerfile-файл, основанный на официальном образе Tomcat с добавленным тестовым приложением https://tomcat.apache.org/tomcat-7.0-doc/appdev/sample/sample.war
./frontend.Dockerfile - файл Dockerfile, основанный на официальном образе httpd, но с обновленными пакетами и установленным «curl».
./docker-compose.yml - описывает конфигурацию двух сервисов (backend - основан на backend.Dockerfile; frontend - основан на frontend.Dockerfile, предоставляет порт 80, использует конфигурацию из «httpd.conf» (/usr/local/apache2/conf/httpd.conf) и «backend.vhost.conf» (mount «/conf/vhosts» в «/usr/local/apache2/conf/vhosts»; healthcheck для обеих служб).




