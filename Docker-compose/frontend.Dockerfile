FROM httpd:2.4

RUN apt-get update && apt-get dist-upgrade \
 && apt-get clean \
 && apt-get install -y \
  curl \
 && rm -rf /var/lib/apt/lists/*

ADD httpd.conf /usr/local/apache2/conf/httpd.conf

CMD ["apachectl", "-k", "start", "-DFOREGROUND"]
